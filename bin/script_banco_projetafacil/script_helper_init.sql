drop database if exists projetafacilmesmo;

create database projetafacilmesmo;

use projetafacilmesmo;




insert into usuario values (1, true, null, 'd@d.com','Admin Projeta', '$2a$06$azDV5oprJJjUhtDQgnqhZ.k8BqCWD4S9kyhyEx7CbHeYlYQgHc1Bi');

insert into grupo values (1, 'ADMINISTRADOR');

insert into usuario_grupo values (1,1);