$('#etapaInsumo').selectize({
	plugins : [ 'remove_button' ],
	maxItems: 30,
});


$('#subEtapaInsumo').selectize({
	plugins : [ 'remove_button' ],
	maxItems: 999
});

$(function() {
	
    $('.atualizar-subetapas').on('click', function(event) {
    	
    	event.preventDefault();
    	
    	var selectize_tags = $("#subEtapaInsumo")[0].selectize;
    	
    	selectize_tags.clearOptions();
    	selectize_tags.refreshItems();
    	
    	var etapas = $('#etapaInsumo').val();
    	
    	if(etapas == '' || null == etapas) { 
    		return;
    	}
    	
    	var dados =  { etapasId : etapas.toString() } ;
    	
    	var path = '/insumos/buscaSubEtapa/';
    
    		var response = $.post(path, dados, function(data) {
    		
    		$(data).each(function() {
    			selectize_tags.addOption({
    				value : this.id, text : this.codigoEtapa + '.' + this.codigo + ' - ' + this.nome
    			});
    			 selectize_tags.addItem(this.codigo);
    		});
    		
    		
    		selectize_tags.refreshItems();

    	}).fail(function(e) {
    		console.log(e);
    	});

    }); // fim do 'on'

});


