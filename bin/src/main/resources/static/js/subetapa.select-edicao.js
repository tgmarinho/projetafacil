var ProjetaFacil = ProjetaFacil || {};

ProjetaFacil.EditarSubEtapa = (function() {
	
	function EditarSubEtapa() {
		this.codigoSubEtapa = $('#labelCodigoSubEtapa');
		this.idEtapa = $('#etapa');
		
	}
	
	EditarSubEtapa.prototype.iniciar = function() {
		if(this.codigoSubEtapa.text() != "") {
			this.idEtapa.attr('readonly',  'readonly');
			this.idEtapa.css('pointer-events','none');
			this.idEtapa.css('touch-action','none');
		}
	}
	
	
	return EditarSubEtapa;
	
}());


$(function() {
	var editarSubEtapa = new ProjetaFacil.EditarSubEtapa();
	editarSubEtapa.iniciar();
});