var ProjetaFacil = ProjetaFacil || {};

ProjetaFacil.ComboEtapa = (function() {
	
	function ComboEtapa() {
		this.combo = $('#etapa');
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	ComboEtapa.prototype.iniciar = function() {
		this.combo.on('change', onEtapaAlterada.bind(this));
	}
	
	function onEtapaAlterada() {
		this.emitter.trigger('alterado', this.combo.val());
	}
	
	return ComboEtapa;
	
}());

ProjetaFacil.ComboSubEtapa = (function() {
	
	function ComboSubEtapa(ComboEtapa) {
		this.ComboEtapa = ComboEtapa;
		this.combo = $('#subEtapa');
		this.imgLoading = $('.js-img-loading');
		this.inputHiddenSubEtapaSelecionada = $('#inputHiddenSubEtapaSelecionada');
		
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	ComboSubEtapa.prototype.iniciar = function() {
		reset.call(this);
		this.combo.on('change', onSubEtapaAlterada.bind(this));
		this.ComboEtapa.on('alterado', onEtapaAlterada.bind(this));
		var idEtapa = this.ComboEtapa.combo.val();
		inicializarSubEtapas.call(this, idEtapa);
	}
	
	function onSubEtapaAlterada() {
		this.emitter.trigger('alterado', this.combo.val());
	}
	
	function onEtapaAlterada(evento, idEtapa) {
		this.inputHiddenSubEtapaSelecionada.val('');
		inicializarSubEtapas.call(this, idEtapa);
	}
	
	function inicializarSubEtapas(idEtapa) {
		if (idEtapa) {
			var resposta = $.ajax({
				url: this.combo.data('url'),
				method: 'GET',
				contentType: 'application/json',
				data: { 'etapa': idEtapa }, 
				beforeSend: iniciarRequisicao.bind(this),
				complete: finalizarRequisicao.bind(this)
			});
			resposta.done(onBuscarSubEtapasFinalizado.bind(this));
		} else {
			reset.call(this);
		}
	}
	
	function onBuscarSubEtapasFinalizado(subEtapas) {
		var options = [];
		
		if(subEtapas == '' || subEtapas.lenght == 0){
			return reset.call(this);
		} else {
			options.push('<option value="">Selecione a sub etapa</option>');
			subEtapas.forEach(function(subEtapa) {
				options.push('<option value="' + subEtapa.id + '">'  + subEtapa.codigo + ' - ' + subEtapa.nome + '</option>');
			});
		}
		
		this.combo.html(options.join(''));
		this.combo.removeAttr('disabled');
		
		var idSubEtapaSelecionada = this.inputHiddenSubEtapaSelecionada.val();
		if (idSubEtapaSelecionada) {
			this.combo.val(idSubEtapaSelecionada);
		}
	}
	
	function reset() {
		this.combo.html('<option value="">Selecione a sub etapa</option>');
		this.combo.val('');
		this.combo.attr('disabled', 'disabled');
	}
	
	function iniciarRequisicao() {
		reset.call(this);
		this.imgLoading.show();
	}
	
	function finalizarRequisicao() {
		this.imgLoading.hide();
	}
	
	return ComboSubEtapa;
	
}());


$(function() {	
	var ComboEtapa = new ProjetaFacil.ComboEtapa();
	ComboEtapa.iniciar();
	
	var ComboSubEtapa = new ProjetaFacil.ComboSubEtapa(ComboEtapa);
	ComboSubEtapa.iniciar();
	
});